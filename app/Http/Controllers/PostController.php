<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Post;
use App\Models\Postlike;
use App\Models\PostComment;

class PostController extends Controller
{
    public function create(){
        return view('posts.create');
    }

    public function store(Request $request){
        if(Auth::user()){
            $post = new Post;
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = (Auth::user()->id);

            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }

    public function index(){
        // $posts = DB::select('select * from posts where isActive = :isActive', ['isActive' => 0]);
          $posts = Post::all()->where('isActive', 1);
         return view('posts.index')->with('posts', $posts);
    }

    public function welcome()
    {
        $posts = Post::inRandomOrder()
                ->where('isActive', 1)
                ->limit(3)
                ->get();
        return view('welcome')->with('posts', $posts);
    }

    public function myPosts(){
        if(Auth::user()){
            $posts = Auth::user()->posts;

            return view('posts.index')->with('posts', $posts);
        }
        else{
            return redirect('/login');
        }
    }

    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    public function editPost($id){
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);
    }

    public function update(Request $request, $id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }
        return redirect('/posts');
    }
    public function destroy($id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            $post->delete();
        }
        return redirect('/posts');
    }

    public function archive($id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            $post->isActive = false;
            $post->save();
        }
        return redirect('/posts');
    }

    public function unarchive($id){
        $post = Post::find($id);

        if(Auth::user()->id == $post->user_id){
            $post->isActive = true;
            $post->save();
        }
        return redirect('/posts');
    }

    public function likes($id){
        $post = Post::find($id);
        if(Auth::user()){
            $user_id = Auth::user()->id;
            if($post->user_id != $user_id){
                if($post->likes->contains("user_id", $user_id)){
                    PostLike::where('post_id', $post->id)->where('user_id', $user_id)->delete();
                }
                else{
                    $postlike = new Postlike;
                    $postlike->post_id = $post->id;
                    $postlike->user_id = $user_id;
                    $postlike->save();
                }
            }
            return redirect("/posts/$id");
        }
        else{
            return redirect('/login');
        }
    }

    public function comments(Request $request, $id){
        $post = Post::find($id);
        if(Auth::user()){
            $user_id = Auth::user()->id;
            if($post->user_id != $user_id){
                $postComment = new PostComment;
                $postComment->post_id = $post->id;
                $postComment->user_id = $user_id;
                $postComment->content = $request->input('content');
                $postComment->save();
            }
            return redirect("/posts/$id");
        }
        else{
            return redirect('/login');
        }
    }

}