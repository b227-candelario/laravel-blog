@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			@if(Auth::id() != $post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					@method('PUT')
					@csrf
					@if($post->likes->contains('user_id', Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
					@else
						<button type="submit" class="btn btn-success">Like</button>
					@endif					
				</form>
				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">Add Comment</button>
			@endif
			<div class="mt-3">
				<a href="/posts" class="card-link">View All Posts</a>
			</div>
		</div>
	</div>

	@if($post->comments->count() > 0)
		<div>Comments:</div>
		@foreach($post->comments as $comment)
			<div class="card mt-1">
				<div class="card-body">
					<div class="text-center">
						{{$comment->content}}
					</div>
				</div>
				<div class="card-footer">
					<div class="text-end">Posted By: {{$comment->user->name}}</div>
					<div class="text-end">Posted On: {{$comment->created_at}}</div>
				</div>
			</div>
		@endforeach
	@endif	

	<!-- MODAL -->
	<form method="POST" action="/posts/{{$post->id}}/comment">		
		@csrf
		<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Add Comment</h5>
		        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		      </div>
		      <div class="modal-body">
		        <label for="content">Content:</label>
		        <textarea class="w-100 rounded" rows="5" name="content" id="content"></textarea>
		      </div>
		      <div class="modal-footer">		        
		        <button type="submit" class="btn btn-primary">Post Comment</button>
		      </div>
		    </div>
		  </div>
		</div>
	</form>
@endsection